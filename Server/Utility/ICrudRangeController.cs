﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Imperium.ServerData.Models;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Provides a generic interface for CRUD range operations.
/// </summary>
namespace Imperium.Server.Utility
{
    public interface ICrudRangeController<D, ID>
    {
        public ActionResult<int> CreateRange(IEnumerable<D> range);
        public ActionResult<D[]> ReadRange(IEnumerable<ID> idRange);
        public ActionResult<int> UpdateRange(IEnumerable<D> range);
        public ActionResult<int> DeleteRange(IEnumerable<ID> idRange);
    }
}
