﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Provides a generic interface for CRUD operations.
/// </summary>
namespace Imperium.Server.Utility
{
    public interface ICrudController<D, ID>
    {
        public ActionResult<D> CreateData(D data);
        public ActionResult<D> ReadDataWithId(ID id);
        public ActionResult<D[]> ReadAllData();
        public ActionResult<D> UpdateData(D data);
        public ActionResult<bool> DeleteDataWithId(ID id);
        public ActionResult<int> DeleteAllData();
    }
}
