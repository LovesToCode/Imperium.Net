﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Imperium.ServerData.Models;
using Imperium.ServerData.Repositories;

namespace Imperium.Server.Services
{
    public class DBLoader : IDBLoader
    {
        private readonly ICommandRepo _cmdRepo;
        private readonly ICommandSetRepo _cmdSetRepo;
        private static bool _cmdTestLoaded = false;
        private static bool _cmdSetTestLoaded = false;

        public DBLoader(ICommandRepo cmdRepo,
                        ICommandSetRepo cmdSetRepo)
        {
            _cmdRepo = cmdRepo;
            _cmdSetRepo = cmdSetRepo;
        }

        public bool LoadCommandTestData()
        {
            List<CommandModel> commands = new();

            for (int x = 0; !_cmdTestLoaded && x < 100; x++)
            {
                CommandModel aCmd = new()
                {
                    Id = $"testcmd-{x}",
                    Set = $"test-set-name-{x}",
                    Type = (CommandModel.CmdType)x,
                    Context = $"This is test content number {x}"
                };
                commands.Add(aCmd);
            }
            if (_cmdRepo.NewCommandRange(commands.ToArray()) > 0)
               _cmdTestLoaded = true;
            else
               _cmdTestLoaded = false;

            return _cmdTestLoaded;
        }

        public bool UpdateCommandTestData()
		{
            List<CommandModel> commands = new();

            for (int x = 0; _cmdTestLoaded && x < 100; x++)
            {
                CommandModel aCmd = new()
                {
                    Id = $"testcmd-{x}",
                    Set = $"UPDATED-set-name-{x}",
                    Type = (CommandModel.CmdType)x,
                    Context = $"This content number {x} has been UPDATED!"
                };

                commands.Add(aCmd);
            }

            return (_cmdRepo.UpdateCommandRange(commands.ToArray()) > 0);
		}

        public bool UnLoadCommandTestData()
        {
            for (int x = 0; _cmdTestLoaded && x < 100; x++)
                if (!_cmdRepo.DeleteCommand($"testcmd-{x}")) break; // Just quit on any error.

            _cmdTestLoaded = false;

            return _cmdTestLoaded;
        }

        public bool LoadCommandSetTestData()
        {
            for (int x = 0; !_cmdSetTestLoaded && x < 100; x++)
            {
                CommandSetModel aCmdSet = new()
                {
                    Id = $"test-command-set-{x}",
                    CommandCount = x*2
                };

                // If any fail, just quit.
                if (!_cmdSetRepo.NewCommandSet(aCmdSet)) break;
            }
            _cmdSetTestLoaded = true;

            return _cmdSetTestLoaded;
        }

        public bool UnLoadCommandSetTestData()
        {
            for (int x = 0; _cmdSetTestLoaded && x < 100; x++)
                if (!_cmdSetRepo.DeleteCommandSet($"test-set-{x}")) break; // Just quit on any error.

            _cmdSetTestLoaded = false;

            return _cmdSetTestLoaded;
        }
    }
}
