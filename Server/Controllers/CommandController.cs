﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Microsoft.AspNetCore.Mvc;
using Imperium.ServerData.Models;
using Imperium.ServerData.Repositories;
using Imperium.Server.Utility;
using Imperium.Server.Services;

/// <summary>
/// Provides for client control of Command objects in the database.
/// </summary>
namespace Imperium.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CommandController : ControllerBase, ICrudController<CommandModel, string>,
                                                     ICrudRangeController<CommandModel, string> 
    {
        private readonly ILogger<CommandController> _logger;
        private readonly ICommandRepo _cmdRepo;
        private readonly IDBLoader _testLoader;

        public CommandController(ILogger<CommandController> logger,
                                 ICommandRepo cmdRepo,
                                 IDBLoader testLoader)
        {
            _logger = logger;
            _cmdRepo = cmdRepo;
            _testLoader = testLoader;
        }

//----- Testing
        [HttpGet("loadtest")]
        public ActionResult<bool> LoadTestData()
        {
            return _testLoader.LoadCommandTestData();
        }

        [HttpGet("updatetest")]
        public ActionResult<bool> UpdateTestData()
        {
            return _testLoader.UpdateCommandTestData();
        }

        [HttpGet("unloadtest")]
        public ActionResult<bool> UnLoadTestData()
        {
            return _testLoader.UnLoadCommandTestData();
        }
//----- Testing

        /// <summary>
        /// Crud: Create a new command in the database.
        /// </summary>
        /// <param name="data">object to store</param>
        /// <returns>result</returns>
        [HttpPost]
        public ActionResult<CommandModel> CreateData(CommandModel data)
        {
            return _cmdRepo.NewCommand(data) ? data : NoContent();
        }

        /// <summary>
        /// cRud: Read a simgle command.
        /// </summary>
        /// <param name="id">unique command name</param>
        /// <returns>the command requested</returns>
        [HttpGet("{id}")]
        public ActionResult<CommandModel> ReadDataWithId(string id)
        {
            CommandModel result = _cmdRepo.GetCommand(id);

            return result is null ? NotFound() : result;
        }

        /// <summary>
        /// cRud: Read all commands.
        /// </summary>
        /// <returns>List of command objects</returns>
        [HttpGet]
        public ActionResult<CommandModel[]> ReadAllData()
        {
            IEnumerable<CommandModel> result = _cmdRepo.GetAllCommands();

            return result.ToArray();
        }

        /// <summary>
        /// crUd: Update a command in the database.
        /// </summary>
        /// <param name="data">command object</param>
        /// <returns>command updated</returns>
        [HttpPut]
        public ActionResult<CommandModel> UpdateData(CommandModel data)
        {
            bool result = _cmdRepo.UpdateCommand(data);

            return result ? data : NoContent();
        }

        /// <summary>
        /// cruD: Delete a command from the database.
        /// </summary>
        /// <param name="id">Unique comand name</param>
        /// <returns>true/valse (pass/fail)</returns>
        [HttpDelete("{id}")]
        public ActionResult<bool> DeleteDataWithId(string id)
        {
            return _cmdRepo.DeleteCommand(id) ? true : NotFound();
        }

        /// <summary>
        /// cruD: Delete all commands from the database.
        /// </summary>
        /// <returns>Number of command deleted</returns>
        [HttpDelete("{id}")]
        public ActionResult<int> DeleteAllData()
        {
            return _cmdRepo.DeleteAllCommands();
        }

        /// <summary>
        /// Create multiple commands at once.
        /// </summary>
        /// <param name="range">List of commands</param>
        /// <returns>number of commands created</returns>
        [HttpPost("{range}")]
        public ActionResult<int> CreateRange(IEnumerable<CommandModel> range)
        {
            return _cmdRepo.NewCommandRange(range.ToArray());
        }

        /// <summary>
        /// Update a range of commands.
        /// </summary>
        /// <param name="range">List of commands</param>
        /// <returns>Number of commands updated</returns>
        [HttpPut("{range}")]
        public ActionResult<int> UpdateRange(IEnumerable<CommandModel> range)
        {
            int result = _cmdRepo.UpdateCommandRange(range.ToArray());

            return result;
        }

        [HttpGet("{range}")]
        public ActionResult<CommandModel[]> ReadRange(IEnumerable<string> idRange)
        {
            IEnumerable<CommandModel> result = _cmdRepo.GetCommandRange(idRange);

            return result.ToArray();
        }

        [HttpDelete("{range}")]
        public ActionResult<int> DeleteRange(IEnumerable<string> idRange)
        {
            throw new NotImplementedException();
        }

        //////////////////
        // Private Methods
/*
        private NotFoundResult _logNotFound(DataResult result)
        {
            _logger.LogError(result.Message);

            return NotFound();
        }
*/
    }
}
