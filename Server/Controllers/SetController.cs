﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Imperium.Server.Services;
using Imperium.Server.Utility;
using Imperium.ServerData.Models;
using Imperium.ServerData.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Imperium.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SetController : ControllerBase, ICrudController<CommandSetModel, string>
    {
        private readonly ILogger<SetController> _logger;
        private readonly ICommandSetRepo _setRepo;
        private readonly IDBLoader _testLoader;

        public SetController(ILogger<SetController> logger,
                             ICommandSetRepo setRepo,
                             IDBLoader testLoader)
        {
            _logger = logger;
            _setRepo = setRepo;
            _testLoader = testLoader;
        }

//----- Testing
        [HttpGet("loadtest")]
        public ActionResult<bool> LoadTestData()
        {
            return _testLoader.LoadCommandSetTestData();
        }

        [HttpGet("unloadtest")]
        public ActionResult<bool> UnLoadTestData()
        {
            return _testLoader.UnLoadCommandSetTestData();
        }
//----- Testing

        [HttpPost]
        public ActionResult<CommandSetModel> CreateData(CommandSetModel data)
        {
            bool result = _setRepo.NewCommandSet(data);

            return result ? data : NotFound();
        }

        [HttpDelete]
        public ActionResult<int> DeleteAllData()
        {
            return _setRepo.DeleteAllCommandSets();
        }

        [HttpDelete("{id}")]
        public ActionResult<bool> DeleteDataWithId(string id)
        {
            return _setRepo.DeleteCommandSet(id);
        }

        [HttpGet]
        public ActionResult<CommandSetModel[]> ReadAllData()
        {
            return _setRepo.GetCommandSets();
        }

        [HttpGet("{id}")]
        public ActionResult<CommandSetModel> ReadDataWithId(string id)
        {
            CommandSetModel result = _setRepo.GetCommandSet(id);

            return result is null ? NotFound() : result;
        }

        [HttpPut]
        public ActionResult<CommandSetModel> UpdateData(CommandSetModel data)
        {
            bool result = _setRepo.UpdateCommandSet(data);

            return result ? data : NotFound();
        }

        //////////////////
        // Private Methods
    }
}
