﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// <summary>
/// Provides interfaces for generic CRUD operations.
/// </summary>
namespace Imperium.Utility.Data
{
    public sealed class CRUD
    {
        public interface ICreate<D>
        {
            D CreateData(D data);
            bool CreateBatch(IEnumerable<D> batch);
        }

        public interface IRead<D, ID>
        {
            D ReadDataWithId(ID id);
            IEnumerable<D> ReadAllData();
            IEnumerable<D> ReadBatch(IEnumerable<ID> idBatch);
        }

        public interface IUpdate<D>
        {
            D UpdateData(D data);
            bool UpdateBatch(IEnumerable<D> batch);
        }

        public interface IDelete<ID>
        {
            bool DeleteWithId(ID id);
            bool DeleteBatch(IEnumerable<ID> idBatch);
            bool DeleteAllData();
        }
    }
}