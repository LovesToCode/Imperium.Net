﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Imperium.ServerData.Models;
using Imperium.ServerData.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Imperium.ServerData.Repositories
{
    /// <summary>
    /// All Commander 'Command Set' related data access methods. This should decouple
    /// the data access from any specific type of service API implementation.
    /// </summary>
    public class CommandSetRepo : ICommandSetRepo
    {
        private readonly ILogger<CommandSetRepo> _logger;
        private readonly ICommandRepo _cmds;

        public CommandSetRepo(ILogger<CommandSetRepo> logger, ICommandRepo cmds)
        {
            _logger = logger;
            _cmds = cmds;
        }

        public int GetNumberOfCommandSets()
        {
            int result = 0;

            using (var db = ServerDBContext.newContext())
            {
                IEnumerable<CommandSetModel> sets;

                sets = db.Sets.AsEnumerable();
                result = sets.Count();
            }

            return result;
        }

        /// <summary>
        /// Create a new command set in the service for organizing commands.
        /// </summary>
        /// <param name="cmdSet"></param>
        /// <returns>DataResult object</returns>
        public bool NewCommandSet(CommandSetModel cmdSet)
        {
            bool result = true;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                try
                {
                    db.Sets.Add(cmdSet);
                    db.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not add command set [{cmdSet.Id}]: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Get a list of all command sets in the service.
        /// </summary>
        /// <returns>Array of command set model objects</returns>
        public CommandSetModel[] GetCommandSets()
        {
            return _getCommandSetsList();
        }

        /// <summary>
        /// Update a command set with new data.
        /// </summary>
        /// <param name="reqSet">New command set data</param>
        /// <returns>DataResult</returns>
        public bool UpdateCommandSet(CommandSetModel reqSet)
        {
            bool result = false;;

            using (var db = ServerDBContext.newContext())
            {
                try
                {
                    db.Update(reqSet);
                    db.SaveChanges();
                    result = true;
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not update command set [{reqSet.Id}]: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);

                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Get a command set from the database.
        /// ** At this time, this acts as a 'ping'
        /// of the command sets for a name, since there's only one column.
        /// More columns could be added later.
        /// </summary>
        /// <param name="setName">The name</param>
        /// <returns>DataResult</returns>
        public CommandSetModel GetCommandSet(string setName)
        {
            if (setName is null)
                return null;
            else
            {
                using (var db = ServerDBContext.newContext())
                {
                    try
                    {
                        return db.Sets.Single(s => s.Id == setName);
                    }
                    catch (DbUpdateException ex)
                    {
                        _logger.LogError($"Could not find command set [{setName}]");
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Delete a single command set from the service database
        /// only if it is empty (no commands are using it).
        /// </summary>
        /// <param name="setName">The set's name</param>
        /// <returns>DataResult object</returns>
        public bool DeleteCommandSet(string setName)
        {
            bool result = true;

            if (_getCommandsInSet(setName).Length == 0)
            {
                using var db = ServerDBContext.newContext();
                try
                {
                    db.Sets.Remove(db.Sets.Single(s => s.Id == setName));
                    db.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not delete command set [{setName}]: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);

                    result = false;
                }
            }

            return result;
            /*

            if (_getCommandsInSet(setName).Count == 0)
            {
                using var db = ServerDBContext.newContext();
                CommandSetModel cmdSet = db.Sets.Single(s => s.Id == setName);

                if (cmdSet != null)
                {
                    try
                    {
                        db.Sets.Remove(cmdSet);
                        db.SaveChanges();

                        result = true;
                        result.Message = $"Command Set [{setName}] was deleted";
                    }
                    catch (DbUpdateException ex)
                    {
                        string logMsg = $"Could not delete command set [{setName}]: {ex.InnerException.Message}";

                        _logger.LogWarning(logMsg);

                        result = false;
                        result.Message = ex.InnerException.Message;
                    }
                }
                else
                {
                    result = false;
                    result.Message = $"Could not find command set [{setName}] for delete";
                }
            }
            else
            {
                result = false;
                result.Message = $"Command Set [{setName}] is not empty. NOT deleted.";
            }

            */
        }

        /// <summary>
        /// Refresh the command sets insures the command counts.
        /// are correct.
        /// </summary>
        /// <returns>DataResult object</returns>
        public bool RefreshCommandSets()
        {
            CommandSetModel[] sets = _getCommandSetsList();

            foreach (CommandSetModel set in sets)
            {
                int nResult = _cmds.GetNumberOfCommands(set.Id);

                if (nResult != set.CommandCount)
                {
                    set.CommandCount = nResult;
                    UpdateCommandSet(set);
                }
            }

            return true;
        }

        /// <summary>
        /// Delete all command sets from the database without
        /// checking if any commands are using them.
        /// </summary>
        /// <returns></returns>
        public int DeleteAllCommandSets()
        {
            int result = 0;
            CommandSetModel[] cmdSets;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                try
                {
                    cmdSets = db.Sets.ToArray();

                    foreach (CommandSetModel set in cmdSets)
                    {
                        db.Sets.Remove(set);
                        result++;
                    }
                    db.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not delete all command sets: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);
                }
            }

            return result;
        }

        /// <summary>
        /// Delete all command sets from the service database
        /// that are not being used by an existing command.
        /// </summary>
        /// <returns>DataResult object</returns>
        public int CleanCommandSets()
        {
            int result = 0;;

            CommandSetModel[] cmdSets;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                cmdSets = db.Sets.ToArray();

                // Delete each individually, which will check for commands using it.
                foreach (CommandSetModel set in cmdSets)
                    if (DeleteCommandSet(set.Id)) result++;
            }

            return result;
        }

        /// <summary>
        /// Delete a command set and all commands within it.
        /// </summary>
        /// <param name="setName">unique name</param>
        /// <returns>Number deleted</returns>
        public int DeleteCommandSetAndCommands(string setName)
        {
            bool cmds_del_ok = true;
            int result = 0;;
            CommandModel[] cmdsInSet = _getCommandsInSet(setName);

            // Delete all the commands in the set first.
            foreach (CommandModel cmd in cmdsInSet)
            {
                if (!_cmds.DeleteCommand(cmd.Id))
                {
                    cmds_del_ok = false;
                    break;
                }
                result++;
            }

            if (cmds_del_ok)
            {
                if (!DeleteCommandSet(setName))
                    _logger.LogWarning($"Problem deleting Command Set [{setName}].");
            }
                
            return result;
        }

        private static CommandSetModel[] _getCommandSetsList()
        {
            List<CommandSetModel> cmdSets;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                cmdSets = new List<CommandSetModel>(db.Sets.AsEnumerable<CommandSetModel>());
            }

            return cmdSets.ToArray();
        }

        private CommandModel[] _getCommandsInSet(string setName)
        {
            CommandModel[] allCmds = _cmds.GetAllCommands();
            List<CommandModel> setCmds = new();

            // Count commands using the set name.
            for (int c = 0; c < allCmds.Length; c++)
            {
                if (allCmds[c].Set.Equals(setName))
                    setCmds.Add(allCmds[c]);
            }

            return setCmds.ToArray();
        }
    }
}
