﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Imperium.ServerData.Database;
using Imperium.ServerData.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Imperium.ServerData.Repositories
{
    /// <summary>
    /// All Commander 'Command' related data access methods. This should decouple
    /// the data access from any specific type of service API implementation.
    /// </summary>
    public class CommandRepo : ICommandRepo
    {
        private readonly ILogger<CommandRepo> _logger;

        public CommandRepo(ILogger<CommandRepo> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get the number of commands in the service that match
        /// the command set name. If the name is null, or 0 length,
        /// the total number of commands in the service are returned.
        /// </summary>
        /// <param name="set">command set name (optional)</param>
        /// <returns>Total number of commands</returns>
        public int GetNumberOfCommands(string set = "")
        {
            int result = 0;

            using (var db = ServerDBContext.newContext())
            {
                IEnumerable<CommandModel> commands;

                if (set != null && set.Length > 0)
                {
                    commands = db.Commands.AsEnumerable().Where(s => s.Set == set);
                }
                else
                {
                    commands = db.Commands.AsEnumerable();
                }

                result = commands.Count();
            }

            return result;
        }

        /// <summary>
        /// Get all commands in the service.
        /// </summary>
        /// <returns>All commands as an array</returns>
        public CommandModel[] GetAllCommands()
        {
            CommandModel[] cmds;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                cmds = db.Commands.ToArray();
            }

            return cmds;
        }

        /// <summary>
        /// Create a new command in the Commander service.
        /// </summary>
        /// <param name="reqData">command to create</param>
        /// <returns>true/false</returns>
        public bool NewCommand(CommandModel reqData)
        {
            bool ret_status = true;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                try
                {
                    db.Commands.Add(reqData);
                    db.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not add command [{reqData.Id}]: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);
                    ret_status = false;
                }
            }

            return ret_status;
        }

        /// <summary>
        /// Add multiple command entries at once.
        /// </summary>
        /// <param name="rangeData">List of commands</param>
        /// <returns>number of commands created (pass/fail)</returns>
        //public DataResult<int> NewCommandRange(IEnumerable<CommandModel> rangeData)
        public int NewCommandRange(CommandModel[] rangeData)
        {
            int result = 0;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                try
                {
                    db.Commands.AddRange(rangeData);
                    db.SaveChanges();

                    result = rangeData.Length;
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Some commands in range may not have been created: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);
                }
            }

            return result;
        }

        /// <summary>
        /// Update the existing command matching the Id in the model object.
        /// Only the type, or context can be changed. Both are set, so they
        /// must match the current values, if they are not being changed.
        /// </summary>
        /// <param name="reqData">command to update</param>
        /// <returns>true/false (pass/fail)</returns>
        public bool UpdateCommand(CommandModel reqData)
        {
            bool ret_status = true;

            using (var db = ServerDBContext.newContext())
            {
                CommandModel aCmd = db.Commands.Single(s => s.Id == reqData.Id);
                if (aCmd != null)
                {
                    aCmd.Type = reqData.Type;
                    aCmd.Set = reqData.Set;
                    aCmd.Context = reqData.Context;

                    try
                    {
                        db.Update(aCmd);
                        db.SaveChanges();
                    }
                    catch (DbUpdateException ex)
                    {
                        string logMsg = $"Could not update command [{reqData.Id}]: {ex.InnerException.Message}";

                        _logger.LogWarning(logMsg);
                        ret_status = false;
                    }
                }
            }

            return ret_status;
        }

        /// <summary>
        /// Update multiple commands.
        /// </summary>
        /// <param name="rangeData">Array of commands</param>
        /// <returns>Number of commands updated</returns>
        public int UpdateCommandRange(CommandModel[] rangeData)
        {
            int result = 0;

            using (var db = ServerDBContext.newContext())
            {
                try
                {
                    db.UpdateRange(rangeData);
                    db.SaveChanges();
                    result = rangeData.Length;
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not update a range of commands: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);
                }
            }

            return result;
        }

        /// <summary>
        /// Delete all commands on the service.
        /// </summary>
        /// <returns>The number of commands deleted</returns>
        public int DeleteAllCommands()
        {
            int result = 0;
            List<CommandModel> cmds;

            using (ServerDBContext db = ServerDBContext.newContext())
            {
                cmds = new List<CommandModel>(db.Commands.AsEnumerable<CommandModel>());

                try
                {
                    foreach (CommandModel cmd in cmds)
                    {
                        db.Commands.Remove(cmd);
                        result++;
                    }

                    db.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    string logMsg = $"Could not delete all commands: {ex.InnerException.Message}";

                    _logger.LogWarning(logMsg);
                }
            }

            return result;
        }

        /// <summary>
        /// Delete a command from the databae.
        /// </summary>
        /// <param name="name">Unique command name</param>
        /// <returns>true/false (pass/fail)</returns>
        public bool DeleteCommand(string name)
        {
            bool ret_status = true;

            using (var db = ServerDBContext.newContext())
            {
                CommandModel aCmd = db.Commands.Single(s => s.Id == name);

                if (aCmd != null)
                {
                    try
                    {
                        db.Commands.Remove(aCmd);
                        db.SaveChanges();
                    }
                    catch (DbUpdateException ex)
                    {
                        string logMsg = $"Could not delete command [{name}]: {ex.InnerException.Message}";

                        _logger.LogWarning(logMsg);
                        ret_status = false;
                    }
                }
            }

            return ret_status;
        }

        /// <summary>
        /// Get a single command from the database.
        /// </summary>
        /// <param name="name">command name to reteieve</param>
        /// <returns>command object</returns>
        public CommandModel GetCommand(string name)
        {
            CommandModel retCmd = null;

            using (var db = ServerDBContext.newContext())
            {
                try
                {
                    //CommandModel aCmd = db.Commands.Single(s => s.Id == name);
                    retCmd = db.Commands.Single(s => s.Id == name);
                }
                catch (InvalidOperationException ex)
                {
                    _logger.LogError($"Command [{name}] was NOT found: [{ex.Message}]");
                }
            }

            return retCmd;
        }

        /// <summary>
        /// Get a range of commands with a list of names. Any names that were
        /// not found are stored in the result object.
        /// </summary>
        /// <param name="names">List of names to find</param>
        /// <returns>List of commands</returns>
        public IEnumerable<CommandModel> GetCommandRange(IEnumerable<string> names)
        {
            List<CommandModel> result = new();

            using (var db = ServerDBContext.newContext())
            {
                CommandModel aCmd;

                foreach (string name in names)
                {
                    try
                    {
                        aCmd = db.Commands.Single(s => s.Id == name);

                        result.Add(aCmd);
                    }
                    catch (InvalidOperationException ex)
                    { 
                        _logger.LogWarning($"Problem retrieving command named [{name}]: " + ex.Message);
                    }
                }
            }

            return result;
        }

        #region Private Stuff

        #endregion
    }
}
