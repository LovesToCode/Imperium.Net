﻿/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using Imperium.ServerData.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;

namespace Imperium.ServerData.Database
{
    /// <summary>
    /// Provides database connectivity for the server.
    /// </summary>
    public class ServerDBContext : DbContext
    {
        //public static string Datasource = "Datasource=ImperiumService.db";
        public static string Datasource = "";

        /// <summary>
        /// Inject the connection string upon creation. Use this in the 
        /// initialization code.
        /// </summary>
        /// <param name="conStr">db connection string</param>
        public ServerDBContext(string conStr)
        {
            if (conStr is null) throw new ArgumentNullException("Connection string cannot be NULL!");
            if (conStr.Length == 0) throw new ArgumentException("Connection string cannot be blank!");

            Datasource = conStr;
        }

        // Used by the factory below to get a context with the current Datasource.
        private ServerDBContext()
        {
            if (Datasource is null) throw new ArgumentNullException("Connection string cannot be NULL!");
        }

        /// <summary>
        /// Simple factory for a new context. Use this after initialization.
        /// </summary>
        /// <returns>new instance</returns>
        public static ServerDBContext newContext()
        {
            return new ServerDBContext();
        }

        /// <summary>
        /// Make an identifier for a UI elements in the database.
        /// </summary>
        /// <param name="target">string to hash</param>
        /// <returns>SHA1 hash</returns>
        public string MakeID(string target)
        {
            byte[] id_buf = Encoding.GetEncoding(0).GetBytes(target);
            SHA1 sha1 = SHA1.Create();
            string hash = BitConverter.ToString(sha1.ComputeHash(id_buf)).Replace("-", "");

            sha1.Dispose();

            return hash;
        }

       public DbSet<CommandModel> Commands { get; set; }
        public DbSet<CommandSetModel> Sets { get; set; }
        /* Later
        public DbSet<ActionModel> Actions { get; set; }
        public DbSet<ActionGroupModel> Groups { get; set; }
        public DbSet<UIButtonPanelModel> Panels { get; set; }
        public DbSet<UIButtonModel> Buttons { get; set; }
        public DbSet<UIButtonTargetModel> Targets { get; set; }
        */

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (Datasource != null)
                optionsBuilder.UseSqlite(Datasource);
            else
                optionsBuilder.UseSqlite("Datasource=ImperiumService.db");
        }
    }
}
